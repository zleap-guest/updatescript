#! /bin/bash
#https://pseudoscripter.wordpress.com/2012/10/16/whiptail-using-a-menu/
#whiptail --title "Backups" --yesno --defaultno "Start the sample program." 8 78
#2nd September 2019
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    status="0"
    while [ "$status" -eq 0 ]  
    do
        choice=$(whiptail --title "Testing" --menu "Make a choice" 16 78 5 \
        "Application 1" "Run apt update" \
        "Application 2" "List upgradable packages" \
        "Application 3" "Run apt upgrade" \
        "Application 4" "Run apt autoremove." \
        "Application 5" "Run apt clean " 3>&2 2>&1 1>&3) 
         
        # Change to lower case and remove spaces.
        option=$(echo $choice | tr '[:upper:]' '[:lower:]' | sed 's/ //g')
        case "${option}" in
            application1) 
                #whiptail --title "Testing" --msgbox "In first option" 8 78
                echo "apt update"
                apt update -y
                sleep 10s
            ;;
            application2)
                #whiptail --title "Testing" --msgbox "In second option" 8 78
                echo "list upgradable"
                apt list --upgradable
                sleep 10s
            ;;
              application3)
                #whiptail --title "Testing" --msgbox "In 3rd option" 8 78
                echo "apt upgrade"
                apt upgrade -y
                sleep 10s
			;;
              application4)
                #whiptail --title "Testing" --msgbox "In 3rd option" 8 78
                echo "auto remove"
                apt autoremove -y
               	sleep 10s
           ;;
            application5)
                #whiptail --title "Testing" --msgbox "In 3rd option" 8 78
                echo "apt clean"
                apt clean -y
                sleep 10s
           ;;        
              *) whiptail --title "Testing" --msgbox "You cancelled or have finished." 8 78
                status=1
                exit
            ;;
        esac
        exitstatus1=$status1
    done
else
    whiptail --title "Testing" --msgbox "You chose not to start." 8 78
    exit
fi
