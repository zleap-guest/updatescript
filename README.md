Update script v 2.0

This update script gives you a menu for the common update options

1. update
2. list upgradable packages
3. upgrade
4. autoremove
5. clean
6. quit

I am still learning shell scripting, found some info on adding menus so added 
to my original updatge script. 

Updated script with menu

run update2.sh as root

I tried to add

if [ "$EUID" -ne 0 ]
  then echo "Please run as root / sudo"
exit

But get " unexpected end of file error "

Added version 1 update.sh for context 

in this case the above root check works.  There seems to be an issue between 
this check and the menu that I added to version 1.

Update3.sh

This adds a whiptail text graphical interface to the program so options can be 
selected from a menu

chkroot.sh

just a script to check you are running as root. As you need to be root to run
updates

Issues

Update.sh checks for root status upon running
Update2 and update3 don't seem to like me doing this (see above notes), 
so the chkroot routine has been removed but could do with 
being in there for completeness

