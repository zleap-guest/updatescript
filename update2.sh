#!/bin/bash
echo "This script MUST be run with root priviledges"
echo
echo "Enter number of the option you would like"
echo
OPTIONS="Update List Upgrade Autoremove Clean Quit"
echo
select opt in $OPTIONS; do
        if [ "$opt" = "Update" ] ; then
            echo Update
            apt update
        elif [ "$opt" = "List" ] ; then
            echo "List Upgradable package"
            apt upgrade
        elif [ "$opt" = "Upgrade" ] ; then
            echo "Upgrade packages"
            apt upgrade -y
        elif [ "$opt" = "Autoremove" ] ; then
            echo "Autoremove packages"
            apt autoremove
        elif [ "$opt" = "Clean" ] ; then
            echo "Clean Up"
            apt clean
        elif [ "$opt" = "Quit" ] ; then
            echo "Thank you and goodbye"
			exit
        else
            echo "bad option"
        fi
    done



