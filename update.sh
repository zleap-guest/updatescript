if [ "$EUID" -ne 0 ]
  then echo "Please run as root / sudo"
  exit

  else
	echo " Run apt update -y "
	apt update -y
	echo "list packages that can be upgraded"
	apt list upgradable
	echo "Run apt upgrade -y (user intervention required)"
	apt upgrade
	echo " Run apt autoremove -y "
	apt autoremove -y
	echo "Run apt clean -y "
	apt clean -y
fi

exit

# help with this script was obtained from :
# https://stackoverflow.com/questions/18215973/how-to-check-if-running-as-root-in-a-bash-script
